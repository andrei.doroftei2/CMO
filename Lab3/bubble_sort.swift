let range = Int(CommandLine.arguments[1]) ?? 0
var data_set = (0..<range).map{ _ in Int.random(in: 1 ... 1000) }
var swap = true
while swap == true {
    swap = false
    for i in 0..<data_set.count - 1 {
        if data_set[i] > data_set[i + 1] {
            let temp = data_set [i + 1]
            data_set[i + 1] = data_set[i]
            data_set[i] = temp
            swap = true
        }
    }
}