let range = Int(CommandLine.arguments[1]) ?? 0
 var data_set = (0..<range).map{ _ in Int.random(in: 1 ... 1000) }
 for i in 0..<data_set.count-1 {
    var minIdx = i
    for j in i+1..<data_set.count {
      if data_set[j] < data_set[minIdx] {
        minIdx = j
      }
    }
    let temp = data_set[i]
    data_set[i] = data_set[minIdx]
    data_set[minIdx] = temp
}
